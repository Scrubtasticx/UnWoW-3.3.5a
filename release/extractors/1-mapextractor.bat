@Echo off
echo.
echo Running mapextractor.exe, logging to mapextractor.log ...
mapextractor.exe 2>&1 > mapextractor.log
echo.
echo Done.
