Before Worldserver.exe can start, you need the dbc, maps, vmaps and mmaps files.
This README should explain how to create them on your own computer.

* Copy ALL extractors (*.exe, *.bat) to your World of Warcraft main folder.
  (The folder where your wow.exe file is stored.)

* Execute the extractors in this order: 

  1) mapextractor.bat
  2) makevmaps4_simple.bat
  3) makemmaps.bat

  The others are run automatically, so don't run any others.

* Put the dbc, maps, vmaps and mmaps folders in the Core folder.
* The core should now boot successfully.

If you prefer, you can download the already extracted files from AC-Web. :)

(If you wonder what the logs are doing in here, I provided them for comparison only.)
