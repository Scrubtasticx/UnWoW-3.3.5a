@Echo off
:vmap4extractor.exe
echo.
echo Running vmap4extractor.exe , logging to vmap4extractor.log ...
vmap4extractor.exe 2>&1 > vmap4extractor.log
:md_vmaps
echo.
echo Creating folder 'vmaps' if it does not exist already.
if not exist vmaps md vmaps
:vmap4assembler.exe
echo.
echo Running vmap4assembler.exe , logging to vmap4assembler.log ...
vmap4assembler.exe Buildings Vmaps 2>&1 > vmap4assembler.log
echo.
echo Done.
