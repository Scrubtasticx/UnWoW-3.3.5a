@Echo off
echo.
echo Creating folder 'mmaps' if it does not already exist.
if not exist mmaps md mmaps
echo.
echo Running mmaps_generator.exe, logging to mmaps_generator.log ...
mmaps_generator.exe 2>&1 > mmaps_generator.log
echo.
echo Done.
